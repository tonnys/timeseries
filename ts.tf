# Set the variable value in *.tfvars file
# or using -var="do_token=..." CLI option
variable "do_token" {}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_ssh_key" "default" {
  name       = "Mac"
  public_key = "${file("/Users/tonnystaunsbrink/.ssh/do.pub")}"
}

data "digitalocean_image" "generic-image" {
  name = "packer-1550405491"
}
# Create a web server
resource "digitalocean_droplet" "graphite-generic" {
  image  = "${data.digitalocean_image.generic-image.image}"
  #"ubuntu-18-10-x64"
  name   = "generic-1"
  region = "lon1"
  size   = "s-1vcpu-3gb"
  ssh_keys = ["${digitalocean_ssh_key.default.fingerprint}"]
  count = 5
}

resource "digitalocean_droplet" "test-client" {
  image  = "ubuntu-18-10-x64"
  #"ubuntu-18-10-x64"
  name   = "test-client"
  region = "lon1"
  size   = "s-1vcpu-1gb"
  ssh_keys = ["${digitalocean_ssh_key.default.fingerprint}"]
  count = 1
}