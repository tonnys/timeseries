#!/bin/sh

export PYTHONPATH="/opt/graphite/lib/:/opt/graphite/webapp/"
apt update
apt install -y python-pip
pip install -y --no-binary=:all: https://github.com/graphite-project/whisper/tarball/master
pip install -y --no-binary=:all: https://github.com/graphite-project/carbon/tarball/master
apt install -y libffi-dev
pip install -y --no-binary=:all: https://github.com/graphite-project/graphite-web/tarball/master

systemctl enable carbon
systemctl start carbon


