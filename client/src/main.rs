extern crate rand;
use std::fmt;
use std::net::TcpStream;
use std::time::SystemTime;
use std::io::Write;
use rand::{Rng, FromEntropy};
use rand::rngs;
use std::ops::Range;
use std::thread;

fn gen_metrics(w: &mut Write, r: Range<i32>) {
    let stats = vec!("max", "p99", "p95", "p75", "min", "std");
    let statuses = vec!("200", "201", "302", "304", "400", "403", "404", "500", "501", "502");
    let mut rng = rngs::StdRng::from_entropy();
    for iw in r {
        for ie in 0..10000 {        
            for stat in &stats {
                for status in &statuses {
                    let v: f32 = rng.gen();//rand::random();
                    let t = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs();
                    //let metric = fmt::format(format_args!("web{}.latency.{}.e{}.{} {} {}", 
                      //  iw, status, ie, stat, v, t));
                    //paths.push(metric);
                    match w.write_fmt(format_args!("web{}.latency.{}.e{}.{} {} {}\n", 
                        iw, status, ie, stat, v, t) ) {
                            Ok(_) => (),
                            Err(e) => panic!(e)
                        }
                }            
            }
        }
    }
}

fn main() {
    //gen_metrics(&mut std::io::stdout());
    let ips = vec!("68.183.38.117", "68.183.38.108", "68.183.38.97", "68.183.42.111", "68.183.42.234");
    let mut count = 0;
    let mut handlers: Vec<thread::JoinHandle<()>> = Vec::new();
    for ip in ips {
        let h = thread::spawn(move || {
            if let Ok(mut stream) = TcpStream::connect(fmt::format(format_args!("{}:2003", ip))) {
                println!("Start Write all {}", ip);
                gen_metrics(&mut stream, (count * 10)..((count + 1) * 10));                
                println!("End Write all {}", ip);
            } else {
                println!("Failed {}", ip);
            }
        });
        handlers.push(h);
        count += 1;
    }
    for handler in handlers {
        handler.join().unwrap();
    }
    return ;
    println!("Starting thread 1");
    let h1 = thread::spawn(|| {
        if let Ok(mut stream) = TcpStream::connect("209.97.185.193:2003") {
            println!("Start Write all 1");
            gen_metrics(&mut stream, 0..25);
            println!("End Write all 1");
        } else {
            println!("Failed");
        }
    });
    println!("Starting thread 2");
    let h2 = thread::spawn(|| {
        if let Ok(mut stream) = TcpStream::connect("134.209.26.185:2003") {
            println!("Start Write all 2");
            gen_metrics(&mut stream, 25..50);
            println!("End Write all 2");
        } else {
            println!("Failed");
        }
    });
    println!("Waiting 1");
    h1.join().unwrap();
    println!("Waiting 2");
    h2.join().unwrap();    
}
